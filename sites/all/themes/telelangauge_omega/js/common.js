function ClearForm(thefield){
    if (thefield.defaultValue==thefield.value)
        thefield.value = ""
    thefield.className+='modified'
} 

jQuery(document).ready(function(){
    jQuery("#panels li").click(function(){
        location.href = $(this).find("a").attr("href");
    });	
    
    var divh = jQuery('.pane-node-content').height();
   
    if(!divh){
      divh = jQuery('.pane-tools-resources').height();
    }
    
    jQuery('.panels-flexible-region-first').css('min-height',divh+"px"); 
    jQuery('.panels-flexible-region-first').find('.inside').append('<div class="gray_background">&nbsp;</div>');
    
    jQuery('.panels-flexible-region-last').css('min-height',divh+"px"); 
    jQuery('.panels-flexible-region-last').find('.inside').append('<div class="gray_background">&nbsp;</div>');
});
