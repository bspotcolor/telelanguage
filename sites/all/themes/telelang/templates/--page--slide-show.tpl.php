<?php
/**
 * @file
 * Zen theme's implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $secondary_menu_heading: The title of the menu used by the secondary links.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['navigation']: Items for the navigation region, below the main menu (if any).
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['footer']: Items for the footer region.
 * - $page['bottom']: Items to appear at the bottom of the page below the footer.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see zen_preprocess_page()
 * @see template_process()
 */
?>
<div>
<div class="telephonic">
		<div class="contain">
			<div class="section group" id="header-top">
				<div class="sec_menu">
					<?php if ($page['header']): ?>
					<?php print render($page['header']); ?>
					<?php endif; ?>
				</div>
			</div><!--#header-top-->
			<div class="section group" id="branding">
				<div class="col span_3_of_7" id="logo">
					<?php if ($logo): ?>
					<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
					<?php endif; ?>
				</div>
  			<div class="colmenu">
  			<?php if ($page['menu']): ?>
  					<?php print render($page['menu']); ?>
              <?php endif; ?>
  			</div>
			</div>
		</div><!--##branding-->
</div><!--.telephonoic-->

<div class="section group" id="main-header-area">
			<div class="contain">
				<div class="col span_4_of_7" id="showcase">
					<div class="header-content">
                    <?php if ($page['banner']): ?>
                    <?php print render($page['banner']); ?>
                    <?php endif; ?>

				    </div>
			</div><!--.contain-->

		</div><!--#main-header-area-->
</div>
		<div id="bucket-area">
      <div class="contain">
  			<div class="section group">
  				<?php if ($page['content_top']): ?>
  				<?php print render($page['content_top']); ?>          
  				<?php endif; ?>
  			</div><!--.contain-->
  			<div class="section group" id="quote-wrap">
  				<div class= "col span_6_of_7" id="quote-contain">
  				<?php if ($page['content_bottom']): ?>
  				<?php print render($page['content_bottom']); ?>
  				<?php endif; ?>
  				</div><!--quote-contain-->
  				<div class="col span_1_of_7" id="contest">
  					<?php if ($page['content_bottom_right']): ?>
  				<?php print render($page['content_bottom_right']); ?>
  				<?php endif; ?>
  				</div><!--contest-->
  			</div><!--.contain-->
     </div>
		</div>
			
		<div class="section group" id="footer">
			<div class="contain">	
				<div id="footer-social">
				<?php if ($page['footer']): ?>
				<?php print render($page['footer']); ?>
				<?php endif; ?>
				</div>
			</div><!--.contain-->	
		</div><!--#footer-->
