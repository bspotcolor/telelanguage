/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {
  var html = '';
  $(document).ready(function(){
    $('.view-price-listing').find('li.views-row').each(function(){
        html = $(this).find('.views-field-title').html();
        $(this).find('.views-field-title').html('');
        $(this).find('.views-field-body .ctools-toggle').html(html);
    });
  });
  
  $("ul.menu li").hover(function() { //Hover over event on list item
	  $(this).css({ 'background' : '#1376c9'}); //Add background color and image on hovered list item
	  $(this).find("ul").show(); //Show the subnav
	 } , function() { //on hover out...
	  $(this).css({ 'background' : 'none'}); //Ditch the background
	  $(this).find("ul").hide(); //Hide the subnav
	});
  

})(jQuery, Drupal, this, this.document);
