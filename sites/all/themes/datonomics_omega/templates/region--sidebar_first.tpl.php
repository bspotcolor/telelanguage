<?php if($content){?>
<aside<?php print $attributes; ?>>
  <div<?php print $content_attributes; ?>>
    <?php print $content; ?>
  </div>
</aside>
<?php }?>