<?php
// $Id: simplenews-multi-block.tpl.php,v 1.6 2009/01/02 12:01:17 sutharsan Exp $

/**
 * @file
 * Default theme implementation to display the simplenews block.
 *
 * Copy this file in your theme directory to create a custom themed block.
 *
 * Available variables:
 * - $subscribed: the current user is subscribed to the $tid newsletter
 * - $user: the current user is authenticated
 * - $message: announcement message (Default: 'Stay informed on our latest news!')
 * - $form: newsletter subscription form
 *
 * @see template_preprocess_simplenews_multi_block()
 */
?>
<div class="datonomics_newsletter">
  <h2 class="newsletter_title">get free email updates</h2>
  <?php if ($message): ?>
    <p><?php print $message; ?></p>
  <?php endif; ?>
  <?php 
  unset($form['name']['#title']);
  unset($form['mail']['#title']);
  $form['name']['#default_value'] = t('Enter your name');
  $form['mail']['#default_value'] = t('Enter your email');
  $form['name']['#attributes'] = array('onblur' => "if(this.value==''){this.value ='Enter your name'}",
                                       'onfocus' => "if(this.value=='Enter your name'){this.value =''}");
  $form['mail']['#attributes'] = array('onblur' => "if(this.value==''){this.value ='Enter your email'}",
                                       'onfocus' => "if(this.value=='Enter your email'){this.value =''}"); 
  $form['update']['#attributes'] = array('class' => array('newsletter_submit'));
  $form['subscribe']['#attributes'] = array('class' => array('newsletter_submit'));
  $form['unsubscribe']['#attributes'] = array('class' => array('newsletter_submit'));
  print render($form); ?>
</div>