<?php
 /**
  * This template is used to print a single field in a view. It is not
  * actually used in default Views, as this is registered as a theme
  * function which has better performance. For single overrides, the
  * template is perfectly okay.
  *
  * Variables available:
  * - $view: The view object
  * - $field: The field handler object that can process the input
  * - $row: The raw SQL result that can be used
  * - $output: The processed output that will normally be used.
  *
  * When fetching output from the $row, this construct should be used:
  * $data = $row->{$field->field_alias}
  *
  * The above will guarantee that you'll always get the correct data,
  * regardless of any changes in the aliasing that might happen if
  * the view is modified.
  */ 
 $popup_content =  ""; 
 if(!empty($row->field_field_media)){ 
  $items = $row->field_field_media[0]['rendered']['#file'];
  if($row->field_field_media[0]['rendered']['#file']->type == 'video'){
    $video = theme('jw_player', array('file_object' => $items, 'preset' => 'jw_preset'));
    $popup_content .= '<a href="?width=650&height=510&inline=true#popup_player-'.$row->nid.'" class="colorbox-inline">Watch Video</a>';
    $popup_content .= '<div style="display:none;"><div id="popup_player-'.$row->nid.'">' . $video .'</div></div>';
  }
  else{
    $image = theme_image(array('path' => $items->uri,'attributes'=>array()));
    $popup_content .= '<a href="?width=auto&height=auto&inline=true#popup_player-'.$row->nid.'" class="colorbox-inline">Watch Image</a>';
    $popup_content .= '<div style="display:none;"><div id="popup_player-'.$row->nid.'">' . $image .'</div></div>';
  }  
 }
  
  $title = 'What you\'ll learn';
  $body = $row->field_body[0]['rendered']['#markup'] . $popup_content;
  print theme('ctools_collapsible', array('handle' => $title, 'content' =>  $body,'collapsed' => true));
