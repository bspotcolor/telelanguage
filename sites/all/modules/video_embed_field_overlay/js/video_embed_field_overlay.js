(function($) {
  var console = window.console || false;
  /**
   * Finds the links to the videos and attaches the overlay behavior
   */
  Drupal.behaviors.videoEmbedFieldOverlay = {
    ready: [],
    timer: null,
    players: {},
    attach: function (context, settings) {
      // Hide all embedded players
      $('.video-overlay-source', context).hide();
      Drupal.behaviors.videoEmbedFieldOverlay.players = $('.video-overlay-source iframe', context);
      
      // Check if domWindow exists. If this doesn't work, nothing else matters
      if ($.isFunction($.openDOMWindow)) {
        // Setup the message handlers
        if (window.addEventListener){ // all browsers except IE before version 9
          window.addEventListener('message', Drupal.behaviors.videoEmbedFieldOverlay.onMessageReceived, false);
        }
        else if (window.attachEvent) { // IE before version 9
          var attached = window.attachEvent('onmessage', Drupal.behaviors.videoEmbedFieldOverlay.onMessageReceived, false);
          if (attached === false) {
            if (window.console) {window.console.log('Failed to attach the listener');}
          }
        }

        // Find all the trigger links
        $('.video-overlay-thumbnail a.overlay', context).bind('click', function(e) {
          // Prevent going to the URL
          e.preventDefault();

          // Get the id of the video
          var id = $(this).attr('href').split('.com/')[1];

          // Trigger our click event
          Drupal.behaviors.videoEmbedFieldOverlay.videoOnClick(id);
          return false;
        });
      }
      else {
        if (window.console) {window.console.log('Cannot create DOMWindow');}
      }
    },
    /**
     * Triggers the DOM Windwow and the autoplay
     */
    videoOnClick: function (id) {
      var f = $('iframe#' + id).parent().get(0) || $('iframe').parent().get(0);
      var iframe = $('iframe#' + id).get(0) || $('iframe').get(0);

      // Setup the DOM Window
      $.openDOMWindow({
        loader:0,
        width:iframe.width || 640,
        height:iframe.height || 360,
        windowSourceID:f
      });

      // Play: Will not work on IE and some older versions of players.
      if (Drupal.behaviors.videoEmbedFieldOverlay.browserSupported() == true) {
        Drupal.behaviors.videoEmbedFieldOverlay.play(id);
      }
    },
    setupListeners: function (id) {
      var f = $('iframe#' + id).get(0) || $('iframe').get(0),
      url = '*';
      if (f.contentWindow.postMessage) {
        f.contentWindow.postMessage({
          method: 'addEventListener', 
          value: 'play'
        }, url);
        f.contentWindow.postMessage({
          method: 'addEventListener', 
          value: 'pause'
        }, url);
        f.contentWindow.postMessage({
          method: 'addEventListener', 
          value: 'finish'
        }, url);
        Drupal.behaviors.videoEmbedFieldOverlay.setStatus(id, 'listening');
        if (window.console) {window.console.log('Listeners setup for ' + id);}
      } else {
        if (window.console) {window.console.log('Browser does not support postMessage');}
      }
    },
    /**
     * Notes:
     * The postMessage method is synchronous in Internet Explorer and 
     * asynchronous in other browsers. 
     */
    play: function (id) {
      var f = $('iframe#' + id).get(0) || $('iframe').get(0),
      url = '*';

      if (Drupal.behaviors.videoEmbedFieldOverlay.getStatus(id) == 'ready') {
        // Setup Listeners
        Drupal.behaviors.videoEmbedFieldOverlay.setupListeners(id);
      }
      if (Drupal.behaviors.videoEmbedFieldOverlay.getStatus(id) !== 'play') {
        if (f.contentWindow.postMessage) {
          // Trigger video play
          f.contentWindow.postMessage({method: 'play', player_id: id}, url);

          Drupal.behaviors.videoEmbedFieldOverlay.timer = setTimeout(function(){
            // Check if video play was acknowledged or else schedule play again
            if (Drupal.behaviors.videoEmbedFieldOverlay.getStatus(id) !== 'play') {
              Drupal.behaviors.videoEmbedFieldOverlay.play(id);
            }
          }, 1000);
        }
        else {
          if (window.console) {window.console.log('Your browser does not support the postMessage method!');}
        }
      }
    },
    /**
     * Handles the messages received by the listener
     */
    onMessageReceived: function(e) {
      var data = jQuery.parseJSON(e.data);
      Drupal.behaviors.videoEmbedFieldOverlay.setStatus(data.player_id, data.event);
    },
    /**
     * Assigns a status to an array of player ids
     */
    setStatus: function (id, status) {
      Drupal.behaviors.videoEmbedFieldOverlay.ready[id] = status;
      if (window.console) {window.console.log('Video: ' + id + ' is ' + status);}
      if (status === 'finish') {
        $.closeDOMWindow();
      }
    },
    /**
     * Retrieves the status for a given player id
     */
    getStatus: function (id) {
      return Drupal.behaviors.videoEmbedFieldOverlay.ready[id];
    },
    browserSupported: function () {
      if ($.browser.msie == true ) {
        if (window.console) {window.console.log('IE does not support the autoplay feature');}
        return false;
      }
      if ($.browser.mozilla == true && $.browser.version.slice(0,3) == '1.9') {
        if (window.console) {window.console.log('Older versions of Mozilla do not support the autoplay feature');}
        return false;
      }
      if ($.browser.opera == true) {
        if (window.console) {window.console.log('Opera does not support the autoplay feature');}
        return false;
      }
      return true;
    }
  }
})(jQuery);
