<?php
// Visit https://code.google.com/apis/console?api=translate to generate your client id, client secret, and to register your redirect uri.
$developer_key='AIzaSyCLjV2e5Ffte629TEkGILDnnFYZajgdyms';
$application_name='Translate API Project';

$max_text_size=256; // Maximum number of characters to translate per request
$enable_captcha=true; // True / False to enable or disable captcha protection
$captcha_req_num=3; // Number of requests from the same visitor IP, after which the captcha is shown
$recaptcha_privatekey="	6Le8qtUSAAAAAPmk1ec6008Om41Ti7m49SAhXzbh"; // Recaptcha private key
$recaptcha_publickey="6Le8qtUSAAAAAHAja6gqetHdvpvUbArKsVHncoRf"; // Recaptcha public key


$text_to_translate = trim(strip_tags($_GET['text_to_translate']));
$translate_from    = $_GET['tr_from'];
$translate_to      = $_GET['tr_to'];

if (!isset($najax)) {
if ($enable_captcha==true) {
 if (is_captcha_needed($captcha_req_num)=="y") {
    if (!isset($_GET["recaptcha_challenge_field"])) { print showcaptcha($recaptcha_publickey, $text_to_translate, $translate_from, $translate_to, "once"); exit; }
    else if (checkcaptcha($recaptcha_privatekey, $_SERVER["REMOTE_ADDR"], $_GET["recaptcha_challenge_field"], $_GET["recaptcha_response_field"])=="y")
    {
        clearrequests($_SERVER["REMOTE_ADDR"]);
        print googletranslate($developer_key, $application_name, $max_text_size, urldecode($text_to_translate), $translate_from, $translate_to); exit;
    }
    else {
        print showcaptcha($recaptcha_publickey, $text_to_translate, $translate_from, $translate_to, "twice"); exit;
    }
 } else { print googletranslate($developer_key, $application_name, $max_text_size, $text_to_translate, $translate_from, $translate_to); exit; }
} else { print googletranslate($developer_key, $application_name, $max_text_size, $text_to_translate, $translate_from, $translate_to); exit; }


}



function googletranslate($developer_key, $application_name, $max_text_size, $text_to_translate, $translate_from, $translate_to) { // Main Translate function

if ( (!$text_to_translate) OR (!$translate_to) ) die(); // Empty request protection
if (mb_strlen($text_to_translate>$max_text_size)) $text_to_translate=mb_substr($text_to_translate,0,$max_text_size); // Protection: cut too long request

include_once 'google-api-php-client/src/apiClient.php';
include_once 'google-api-php-client/src/contrib/apiTranslateService.php';

$client = new apiClient();
$client->setApplicationName($application_name);
$client->setDeveloperKey($developer_key);
$service = new apiTranslateService($client);
$translations = $service->translations->listTranslations($text_to_translate, $translate_to);
$result=$translations['translations'][0]['translatedText'];
$detlan=$translations['translations'][0]['detectedSourceLanguage'];

return $result;
}

function is_captcha_needed($captcha_req_num) { // This function checks if user reached captcha limit and adds +1 view if needed
$iu=0; $result="n"; $used=0;
$limf=file_get_contents("filecache/limits.txt");
$limfa=explode(",",$limf);
foreach ($limfa as &$lima){
 $lim=explode("|",$lima);
 if ($_SERVER["REMOTE_ADDR"]==$lim[0]) {
  if ($lim[1]>=$captcha_req_num) { $iu=1; $result="y"; break; }
  else { $used=$lim[1]+1; $limf=str_replace("$lim[0]|$lim[1]","$lim[0]|$used", $limf); file_put_contents("filecache/limits.txt",$limf); $result="n"; $iu=1; break; }
 }
}
if ($iu==0) { $limf.=$_SERVER["REMOTE_ADDR"]."|1,"; file_put_contents("filecache/limits.txt",$limf); }

return $result;
}

function showcaptcha($publickey, $text_to_translate, $translate_from, $translate_to, $chk) { // This function draws captcha
require_once('recaptchalib.php');
if ($chk=="twice") $chk="Captcha entered incorrectly<br/>"; else $chk="";
$msg="<form method=\"get\" name=\"f\" action=\"\" onsubmit=\"teletranslate(); return false;\">
<p style=\"text-align:center;margin-top:30px;\">".$chk."Please type the 2 words in the image:</p>
<input type=\"hidden\" id=\"c_tt_text\" name=\"text_to_translate\" value=\"$text_to_translate\"/>
<input type=\"hidden\" id=\"c_tt_from\" name=\"tr_from\" value=\"$translate_from\"/>
<input type=\"hidden\" id=\"c_tt_to\" name=\"tr_to\" value=\"$translate_to\"/>
<div style=\"width:100%;height:130px;margin:20px;text-align:center;\">".recaptcha_get_html($publickey)."</div>
</form>";
return $msg;
}

function checkcaptcha($privatekey, $remote_addr, $recaptcha_challenge_field, $recaptcha_response_field){ // This function checks capthcha input
$result="n";
require_once('recaptchalib.php');
$resp = recaptcha_check_answer ($privatekey, $remote_addr, $recaptcha_challenge_field, $recaptcha_response_field);
if (!$resp->is_valid) $result="n";   // Wrong captcha
else $result="y"; // Correct captcha
return $result;
}

function clearrequests($ip){
$limf=file_get_contents("filecache/limits.txt");
if (strlen($limf>(1048576*1))) file_put_contents("filecache/limits.txt",","); // Clear limits file if it's size is more then 1M
else {
 $p1=strpos($limf,"$ip|");
 $p2=strpos($limf,",",$p1)+1;
 $s=substr($limf,$p1,($p2-$p1));
 $limf=str_replace($s,"",$limf);
 file_put_contents("filecache/limits2.txt",$s);
 file_put_contents("filecache/limits.txt",$limf);
}
return true;
}

?>