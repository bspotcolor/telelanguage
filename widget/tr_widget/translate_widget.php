<?

include_once('languages.php');

$widget_js="<script type=\"text/javascript\"><!--
function swaplanguages(){
var tt_fromv=document.getElementById('tt_from').options[document.getElementById('tt_from').selectedIndex].value;
var tt_from=document.getElementById('tt_from').selectedIndex;
var tt_to=document.getElementById('tt_to').selectedIndex;
if (tt_fromv==\"auto\") { return false; }
else if (tt_fromv==\"zh-TW\") { document.getElementById('tt_to').selectedIndex=11; document.getElementById('tt_from').selectedIndex=tt_to; }
else { document.getElementById('tt_to').selectedIndex=tt_from; document.getElementById('tt_from').selectedIndex=tt_to; }
}
function textCounter(field,cntfield,maxlimit) {
if (field.value.length > maxlimit) field.value = field.value.substring(0, maxlimit); // if too long...trim it!
else cntfield.value = maxlimit - field.value.length+ ' characters remaining'; // otherwise, update 'characters left' counter
}
function teletranslate(){
// no-captcha
if (document.forms.f == undefined){
var tt_text=document.getElementById('text_from').value;
var tt_from=document.getElementById('tt_from').options[document.getElementById('tt_from').selectedIndex].value;
var tt_to=document.getElementById('tt_to').options[document.getElementById('tt_to').selectedIndex].value;
if (tt_text=='') {alert('Please type a text to translate');}
else {
var rdf=document.getElementById('text_to');
if(window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();}
else{xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");}
xmlhttp.onreadystatechange=function(){
if(xmlhttp.readyState==4 && xmlhttp.status==200){
var sub =xmlhttp.responseText.substr(0,4);
if (sub !='<for') rdf.innerHTML=xmlhttp.responseText;
else window.location.href='/widget/?op=captcha&text_to_translate='+tt_text+'&tr_from='+tt_from+'&tr_to='+tt_to;
}}
xmlhttp.open(\"GET\",\"tr_widget/ajax_gtranslate.php?text_to_translate=\"+tt_text+\"&tr_from=\"+tt_from+\"&tr_to=\"+tt_to,true);
xmlhttp.send(); return false; }
}
else {
// captcha
var tt_text=document.forms.f.text_to_translate.value;
var tt_from=document.forms.f.tr_from.value;
var tt_to=document.forms.f.tr_to.value;
var recaptcha_response_field=document.forms.f.recaptcha_response_field.value;
var recaptcha_challenge_field=document.forms.f.recaptcha_challenge_field.value;
if (tt_text=='') {alert('Please type a text to translate');}
else {
var rdf=document.getElementById('text_to');
if(window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();}
else{xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");}
xmlhttp.onreadystatechange=function(){
if(xmlhttp.readyState==4 && xmlhttp.status==200){
var sub =xmlhttp.responseText.substr(0,4);
if (sub !='<for') { rdf.innerHTML=xmlhttp.responseText; document.body.removeChild(document.getElementById('capf')); document.body.removeChild(document.getElementById('capb')); document.getElementById('text_from').value=tt_text; }
else {window.location.reload();}
}}
xmlhttp.open(\"GET\",'tr_widget/ajax_gtranslate.php?text_to_translate='+tt_text+'&tr_from='+tt_from+'&tr_to='+tt_to+'&recaptcha_response_field='+recaptcha_response_field+'&recaptcha_challenge_field='+recaptcha_challenge_field,true);
xmlhttp.send(); return false; }
}
return false;
}
--></script>\n";

$widget_css="<style type=\"text/css\">
#tr_widget{width:544px;height:260px;background-color:#9c805b;color:#fff;padding:10px;}
#tr_widget .tdt{width:100%;height:40px;font-size:20px;color:#fff;font-family:Stamp Act,Sans Serif;font-weight:600;}
#tr_widget .tdl{width:205px;}
#tr_widget .tdm{width:125px;text-align:center;}
#tr_widget .tdr{width:205px;}
#tr_widget .tdb{width:544px;font-family:Arial;font-size:12px;font-weight:500;color:#fff;padding-top:5px;}
#tr_widget .tt_text{width:200px;height:140px;}
#tr_widget .tt_rotate{width:28px;height:21px;vertical-align:top;}
#tr_widget select{font-size:11px;width:110px;}
#tr_widget #ttbutton{width:115px;height:35px;margin:15px 0 0 0;padding:15px 0 0 0;background-color:#7b579d;text-align:center;font-size:16px;font-weight:bold;color:#fff;cursor:pointer;}
#tr_widget #text_l{width:130px;margin:0 0 -7px 2px;border:0;font-size:10px;color:#6c6c6c;background-color:#9c805b;}
.capb{position:fixed;left:0;top:0;opacity:0.5;-moz-opacity:0.5;filter:alpha(opacity=50);width:100%;height:100%;background-color:#000;left:0;top:0;margin-top:0;z-index:1;overflow:hidden;}
.capf{position:fixed;left:36%;top:10%;width:350px;height:210px;background-color:#fff;z-index:2;overflow:hidden;-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;background:#fff none repeat scroll 0 0;margin-bottom:14px;border:4px solid #9c805b;}
}
</style>\n";

$widget_html="
<div id=\"tr_widget\">
<table>
<tr><td colspan=\"3\" class=\"tdt\">FREE TRANSLATION <i>{ <span style=\"font-family:Arial;font-size:14px;font-weight:700;\">To translate type or paste text below</span> }</i><br/><input type=\"text\" id=\"text_l\" value=\"256 characters remaining\" disabled=\"disabled\"/></td></tr>
<tr>
<td class=\"tdl\"><textarea class=\"tt_text\" id=\"text_from\" rows=\"\" cols=\"\" onkeydown=\"textCounter(document.getElementById('text_from'),document.getElementById('text_l'),256)\" onkeyup=\"textCounter(document.getElementById('text_from'),document.getElementById('text_l'),256)\"></textarea></td>
<td class=\"tdm\">
<table>
<tr><td>$select_tr_from</td></tr>
<tr><td>To: <a href=\"#\" title=\"Rotate Languages\" onclick=\"swaplanguages(); return false;\"><img src=\"tr_widget/rotate_languages.png\" alt=\"Rotate languages\" class=\"tt_rotate\"/></a></td></tr>
<tr><td>$select_tr_to<br/><div id=\"ttbutton\" onclick=\"teletranslate(); return false;\">Translate</div></td></tr>
</table>
</td>
<td class=\"tdr\"><textarea class=\"tt_text\" id=\"text_to\" rows=\"\" cols=\"\" readonly=\"readonly\"></textarea></td></tr>
<tr><td colspan=\"3\" class=\"tdb\">Use Telelanguage online language translator to quickly understand the information you need in real-time. Be it for personal or business use, Telelanguage's free online translation service lets you translate any text in the language of your choice. The online translator is quick and easy-to-use!</td></tr>
</table>
</div>\n";

if (isset($_GET['op']) AND ($_GET['op']=="captcha")) {
$najax=true;
include_once('ajax_gtranslate.php');
$text_to_translate=trim(strip_tags($_GET['text_to_translate']));
$translate_from=$_GET['tr_from'];
$translate_to=$_GET['tr_to'];
$widget_html.="<div class=\"capb\" id=\"capb\"></div><div class=\"capf\" id=\"capf\">".showcaptcha($recaptcha_publickey, $text_to_translate, $translate_from, $translate_to,"once")."</div>";
}

print $widget_css;
print $widget_js;
print $widget_html;


?>