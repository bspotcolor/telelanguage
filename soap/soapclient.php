<?php 
//http://users.skynet.be/pascalbotte/rcx-ws-doc/phpclient.htm

require_once('nusoap.php');

$client = new nusoap_client('https://www.interpreterplatform.com/CFC/SOAP/registration.cfc?wsdl', 'wsdl');
$err = $client->getError();
if ($err) {
	echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
}
echo '4';
//$param = array('customerName' => 'Telelanguage');
$param = array( 'rateplan_code'=> '1',
			'rateplan_amount' => '1' ,
			'bizdata_company_name' => 'Greg',
			'bizdata_first_name' => 'Greg', 
			'bizdata_last_name' => 'Greg',
			'bizdata_title' => 'Greg',
			'bizdata_phone' => '12456790',
			'bizdata_fax' => '124353656',
			'bizdata_url' => '124546467678',
			'bizdata_address' => '123 portland street',
			'bizdata_address2' => '',
			'bizdata_city' => 'Portland',
			'bizdata_state' => 'Oregon',
			'bizdata_zipcode' => '19456',
			'bizdata_timezone' => '4',
			'bizdata_service_type' => '1',
			'bizdata_migration' => '1',
			
			//'bizdata_email' =>
			//'bizdata_password' =>
			'EmailAddress' => 'greg@humhost.com',
			'Email_Address' => 'greg@humhost.com',
			'Password' => 'blahblah',
			'credit_first_name' => 'greg',
			'credit_last_name' => 'boggs',
			'credit_billing_address' => '1234 portland street',
			'credit_billing_address2' =>'',
			'credit_billing_city' =>' portland',
			'credit_billing_state' => 'OR',
			'credit_billing_zipcode' =>'97214',
			'credit_card_number' => '12343567678',
			'credit_card_type' => 'visa',
			'credit_expires_month' => '01',
			'credit_expires_year' => '2012',
			'credit_card_code' => '822',
			'credit_authorization' => '1'
		);
echo "<h2> Data </h2>";
print_r($param);

$proxy = $client->getProxy();

$result = $proxy->insertcustomer($param);
echo '<h1>Return</h1>';
print_r($result);
	
if($client->fault) {
	echo "FAULT: <p>code: {$proxy->faultcode}<br />";
    echo "String: {$proxy->faultstring}<br />";
    echo '<pre>' . htmlspecialchars($proxy->request, ENT_QUOTES) . '</pre>';
    echo '<pre>' . htmlspecialchars($proxy->response, ENT_QUOTES) . '</pre>';
} else {
    echo '<h1>Debug</h1>';
    echo '<h2>Request</h2>';
    echo '<pre>' . htmlspecialchars($proxy->request, ENT_QUOTES) . '</pre>';
    echo '<h2>Response</h2>';
    echo '<pre>' . htmlspecialchars($proxy->response, ENT_QUOTES) . '</pre>';
}
?>  
