<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1');

require_once('../nusoap.php');

$status = '';
$wsdl = 'https://www.interpreterplatform.com/CFC/SOAP/registration.cfc?wsdl';
$company = $_POST['bizdata_company_name'];
$email = $_POST['email_address'];
$client = new nusoap_client($wsdl, 'wsdl');
$error = false; 

$err = $client->getError();
if ($err) {
	echo '<h2>Error submitting your order. Please call 888.983.5352</h2><pre>' . $err . '</pre>';
}

$proxy = $client->getProxy();
if ($company) {
	$result = $proxy->ValidateCompanyName($company);

	if($client->fault) {
    	$status .= "FAULT code: addslashes($proxy->faultcode)<br />";
    	$status .= "String: addslashes($proxy->faultstring)<br />";
    	$status .= '<pre>' . $proxy->request . '</pre>';
    	$status .= '<pre>' . $proxy->response . '</pre>';
	} else {
    	$status .= '<h2>Request</h2>';
    	$status .= '<pre>' . $proxy->request . '</pre>';
    	$status .= '<h2>Response</h2>';
    	$status .= '<pre>' . $proxy->response . '</pre>';
		if ($result) {
			$json = '{"error" : true, "location" : "#company_status", "errormessage" : "<span class=\"error\">' . $company . ' is already taken. This name must be unique.</span>"}';
		}
	}
}
//echo $status;

if ($email) {
	$result = $proxy->ValidateUserEmail($email);

	if($client->fault) {
    	$status .= "FAULT code: addslashes($proxy->faultcode)<br />";
    	$status .= "String: addslashes($proxy->faultstring)<br />";
    	$status .= '<pre>' . $proxy->request . '</pre>';
    	$status .= '<pre>' . $proxy->response . '</pre>';
	} else {
    	$status .= '<h2>Request</h2>';
    	$status .= '<pre>' . $proxy->request . '</pre>';
    	$status .= '<h2>Response</h2>';
    	$status .= '<pre>' . $proxy->response . '</pre>';
		if ($result) {
			$json = '{"error" : true, "location" : "#email_status", "errormessage" : "<span class=\"error\">' . $email . ' is already taken. This must be unique.</span>"}';
		} 
	}
}
	if ( !isset($json) ) {
		$json = '{"error" : false}';
	}
	
//echo $status;
echo $json;
?>
