<?php 
//http://users.skynet.be/pascalbotte/rcx-ws-doc/phpclient.htm
require_once('../nusoap.php');
//print_r($_POST);

$status = '';
unset($_POST['x']);
unset($_POST['y']);
$wsdl = 'https://www.interpreterplatform.com/CFC/SOAP/registration.cfc?wsdl';
$client = new nusoap_client($wsdl, 'wsdl');

$err = $client->getError();
if ($err) {
	echo '<h2>Error submitting your order. Please call 888.983.5352</h2><pre>' . $err . '</pre>';
}

$proxy = $client->getProxy();
$result = $proxy->InsertCustomer($_POST);

if($client->fault) {
    $status .= "FAULT: <p>code: {$proxy->faultcode}<br />";
    $status .= "String: {$proxy->faultstring}<br />";
    $status .= '<pre>' . htmlspecialchars($proxy->request, ENT_QUOTES) . '</pre>';
    $status .= '<pre>' . htmlspecialchars($proxy->response, ENT_QUOTES) . '</pre>';

} else {
    $status .= '<h1>Debug</h1>';
    $status .= '<h2>Request</h2>';
    $status .= '<pre>' . htmlspecialchars($proxy->request, ENT_QUOTES) . '</pre>';
    $status .= '<h2>Response</h2>';
    $status .= '<pre>' . htmlspecialchars($proxy->response, ENT_QUOTES) . '</pre>';
}
//echo $status;

//echo '<h1>Result</h1>';

if ($result) {
    echo '<h2>Thank You for Your Order</h2>';
    echo '<p><strong>You have successfully registered.</strong> <a href="https://www.interpreterplatform.com/gip/index.cfm">Click here</a> to login to your new account. </p>' .
       	'<strong>Your user:'. $_POST['email_address'] . '</strong><br />' .
		'<strong>Your pass:'. $_POST['password'] . '</strong><br />';
} else {
	echo 'Your registration failed. Please call <strong>888.983.5352</strong> <br />.';
}
//echo '{"statusmessage" : "' . $status . '"}';

?>
